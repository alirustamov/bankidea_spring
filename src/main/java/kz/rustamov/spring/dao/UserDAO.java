package kz.rustamov.spring.dao;

import java.util.List;

import kz.rustamov.spring.model.User;
 
public interface UserDAO {
    public List<User> list();
}